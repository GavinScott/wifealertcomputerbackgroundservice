import argparse
import boto3
import json
import time
import os
from subprocess import Popen

alarmOkValue = 'OK'

parser = argparse.ArgumentParser()
parser.add_argument(
    '--chromePath',
    required=True,
    help='absolute path to the chrome executable on your computer')
parser.add_argument(
    '--htmlPath',
    required=True,
    help='absolute path to alert.html on your computer')
parser.add_argument(
    '--audioPlayerPath',
    required=True,
    help='absolute path to an audio player (like VLC) on your computer')
args = parser.parse_args()

with open('creds.json', 'r') as f:
    creds = json.load(f)

session = boto3.Session(
    aws_access_key_id=creds['aws_access_key_id'],
    aws_secret_access_key=creds['aws_secret_access_key'],
    region_name=creds['region']
)
ssm = session.client('ssm')

displayedAlert = False

while True:
    param = ssm.get_parameter(
        Name='WifeAlertParam',
        WithDecryption=True
    )
    if param['Parameter']['Value'] == alarmOkValue:
        if displayedAlert:
            print('No longer in alert state')
            displayedAlert = False
    elif not displayedAlert:
        print('Displaying alert!')
        displayedAlert = True
        Popen([args.audioPlayerPath, 'goat.mp3', '--qt-start-minimized'])
        os.system(f'{args.chromePath} --start-fullscreen {args.htmlPath}')
    time.sleep(2)
