# README

This is a script that is intended to run in the background of any computer with a monitor and speakers. It continuously polls the SSM parameter and if it finds that it has changed from "OK", it opens `index.html` in a fullscreen Chrome browser and plays `goat.mp3` in the background.

## Dependencies

- boto3
- argparse
- Google Chrome
- Some audio player (like VLC)

## Setup

1. This will not work until you've deployed the WifeAlert CloudFormation stack to an AWS account and have an access/secret key pair for the `WifeAlertPollingUser`.
2. Create a file in the same directory as `wife-alert-background-service-driver.py` called `creds.json`. It should look like this:

```json
{
  "aws_access_key_id": "YOUR_ACCESS_KEY",
  "aws_secret_access_key": "YOUR_SECRET_KEY",
  "region": "YOUR_AWS_REGION"
}
```

3. Run `wife-alert-background-service-driver.py` in the background, providing it with absolute paths to `index.html`, `chrome.exe` and an audio player (I use VLC).

- When I start the process, I run this command (running on Windows subsystem for Linux):

  ```bash
  nohup python3 wife-alert-background-service-driver.py \
    --chromePath "/mnt/c/Program Files\ \(x86\)/Google/Chrome/Application/chrome.exe" \
    --htmlPath "file:///C:/Users/gavin/Documents/linuxFS/wife-alert/wifealertcomputerbackgroundservice/alert.html" \
    --audioPlayerPath "/mnt/c/Program Files/VideoLAN/VLC/vlc.exe" &
  ```

4. Wait for your wife to push the button.

## Stopping the service

If you want to kill the service you can find its process id by running `ps` and killing it, or run `kill.sh` which will do it for you.
